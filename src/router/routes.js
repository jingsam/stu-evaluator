
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'tasks', component: () => import('pages/Tasks.vue') },
      { path: 'score', component: () => import('pages/Score.vue') },
      { path: 'results', component: () => import('pages/Results.vue') },
      { path: 'results2', component: () => import('pages/Results2.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
